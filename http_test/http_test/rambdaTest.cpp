#include "rambdaTest.h"

rambdaTest::rambdaTest(string _aa):aa(aa)
{
    func11(aa);
}

void rambdaTest::func11(string& bb)
{
    func22(bb, [&](string dd) mutable {
        string ee = dd + " 2 " + bb;
        });
}

void rambdaTest::func22(string& cc, function<void(string)> callback = nullptr)
{
    cc += "1";
    callback(cc);
}