#pragma once
#include <iostream>
#include <string>
#include <cpprest/http_client.h>
#include <locale>

#if _DEBUG
#pragma comment (lib, "cpprest142_2_10d.lib")
#else
#pragma comment (lib, "cpprest142_2_10.lib")
#endif

using namespace std;
using namespace web::http::client;

class httpBard
{
	wstring url = L"http://google.com";
	
public:
	httpBard();
	void getHttp();
};

