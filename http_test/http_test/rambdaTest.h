#pragma once
#include <string>
#include <functional>

using namespace std;

class rambdaTest
{
    string aa;
public:
	rambdaTest(string _aa);
    void func11(string& bb);
    void func22(string& cc, function<void(string)> callback);
};

