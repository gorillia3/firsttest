#include "httpBard.h"

httpBard::httpBard()
{
	getHttp();
}

void httpBard::getHttp()
{
	http_client client(url);
	auto resp = client.request(L"GET").get();

	int status_code = resp.status_code();
	wstring content_type = resp.headers().content_type();
	wstring extract_string = resp.extract_string(true).get();
}